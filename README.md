# Euler_LSF_cluster_availability

Script to get an overview on the current view that LSF has of the Euler cluster

```
[sfux@eu-login-45 bin]$ cpua.sh

*=============== LSF Cluster Availability ===============
| Hosts total                                  : 2504
| Hosts in production                          : 2460
| Hosts out of production                      : 44
*--------------------------------------------------------
| eu-ms out of production                      : 27 out of 1215
| hostnames                                    : eu-ms-001-03,eu-ms-001-05,eu-ms-001-36,eu-ms-002-14,eu-ms-003-12,eu-ms-005-40,eu-ms-006-05,eu-ms-008-37,eu-ms-009-02,eu-ms-009-19,eu-ms-009-26,eu-ms-010-06,eu-ms-011-10,eu-ms-012-33,eu-ms-013-15,eu-ms-013-22,eu-ms-014-24,eu-ms-017-05,eu-ms-017-41,eu-ms-018-06,eu-ms-020-32,eu-ms-021-07,eu-ms-021-44,eu-ms-023-08,eu-ms-023-14,eu-ms-024-22,eu-ms-025-40,
| empty eu-ms hosts                            : 230 out of 1215
*--------------------------------------------------------
| eu-c7 out of production                      : 0 out of 352
| hostnames                                    :
| empty eu-c7 hosts                            : 17 out of 352
*--------------------------------------------------------
| eu-a6 out of production                      : 0 out of 339
| hostnames                                    :
| empty eu-a6 hosts                            : 18 out of 339
*--------------------------------------------------------
| eu-fn out of production                      : 6 out of 15
| hostnames                                    : eu-fn-2t-02,eu-fn-2t-04,eu-fn-2t-05,eu-fn-2t-08,eu-fn-4t-01,eu-fn-4t-02,
| empty eu-fn hosts                            : 1 out of 15
*--------------------------------------------------------
| eu-g1 out of production                      : 0 out of 216
| hostnames                                    :
| empty eu-g1 hosts                            : 7 out of 216
*--------------------------------------------------------
| eu-g2 out of production                      : 1 out of 20
| hostnames                                    : eu-g2-08,
| empty eu-g2 hosts                            : 12 out of 20
*--------------------------------------------------------
| eu-g3 out of production                      : 0 out of 59
| hostnames                                    :
| empty eu-g3 hosts                            : 16 out of 59
*--------------------------------------------------------
| eu-a2p out of production                      : 10 out of 288
| hostnames                                    : eu-a2p-018,eu-a2p-034,eu-a2p-040,eu-a2p-058,eu-a2p-083,eu-a2p-126,eu-a2p-132,eu-a2p-142,eu-a2p-230,eu-a2p-232,
| empty eu-a2p hosts                            : 115 out of 288
*--------------------------------------------------------
| Cores total                                  : 100216
| Cores in production                          : 98024
| Cores out of production                      : 2192
*--------------------------------------------------------
| Cores in use                                 : 52591
| Cores available                              : 45433
| Pending job slots                            : 157805
*--------------------------------------------------------
| Cores out of production % over total         : 2.18%
| Cores out of production % over in prod       : 2.23%
| Cores in use % over in production            : 53.65%
*--------------------------------------------------------
| Hosts out of production % over total         : 1.75%
| Hosts out of production % over in production : 1.78%
*========================================================

[sfux@eu-login-45 bin]$
```
