#!/bin/bash

# uncomment for debugging
# set -x

# Save bhosts and bqueues output in files
TDIR=${HOME}/bin
bhosts -w > $TDIR/bhostw.txt
bqueues > $TDIR/bqueues.txt


# Total number of cores and hosts (IP+OOP)
hostsms=$(( $( cat $TDIR/bhostw.txt | grep eu-ms | wc -l ) ))
hostsa6=$(( $( cat $TDIR/bhostw.txt | grep eu-a6 | wc -l ) ))
hostsc7=$(( $( cat $TDIR/bhostw.txt | grep eu-c7 | wc -l ) ))
hostsfn=$(( $( cat $TDIR/bhostw.txt | grep eu-fn | wc -l ) ))
hostsg1=$(( $( cat $TDIR/bhostw.txt | grep eu-g1 | wc -l ) ))
hostsg2=$(( $( cat $TDIR/bhostw.txt | grep eu-g2 | wc -l ) ))
hostsg3=$(( $( cat $TDIR/bhostw.txt | grep eu-g3 | wc -l ) ))
hostsa2p=$(( $( cat $TDIR/bhostw.txt | grep eu-a2p | wc -l ) ))

# Number of empty hosts
emptyms=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-ms | wc -l ) ))
emptya6=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-a6 | wc -l ) ))
emptyc7=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-c7 | wc -l ) ))
emptyfn=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-fn | wc -l ) ))
emptyg1=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-g1 | wc -l ) ))
emptyg2=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-g2 | wc -l ) ))
emptyg3=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-g3 | wc -l ) ))
emptya2p=$(( $( cat $TDIR/bhostw.txt | grep "0      0      0      0      0" | grep eu-a2p | wc -l ) ))

# Sum up number of hosts and cores
hosts_total=$(( $hostsms + $hostsa6 + $hostsc7 + $hostsfn + $hostsg1 + $hostsg2 + $hostsg3 + $hostsa2p ))
cores_total=$(( $hostsms*4 + $hostsa6*36 + $hostsc7*24 + $hostsfn*128 + $hostsg1*128 + $hostsg2*36 + $hostsg3*128 + $hostsa2p*128 ))
cores_g1_total=$(( $hostsg1*128 ))

# Total number of cores and hosts OOP
hostsmsoop=$(( $( cat $TDIR/bhostw.txt | grep eu-ms | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsa6oop=$(( $( cat $TDIR/bhostw.txt | grep eu-a6 | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsc7oop=$(( $( cat $TDIR/bhostw.txt | grep eu-c7 | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsfnoop=$(( $( cat $TDIR/bhostw.txt | grep eu-fn | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsg1oop=$(( $( cat $TDIR/bhostw.txt | grep eu-g1 | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsg2oop=$(( $( cat $TDIR/bhostw.txt | grep eu-g2 | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsg3oop=$(( $( cat $TDIR/bhostw.txt | grep eu-g3 | grep -v 'closed_Full\|ok' | wc -l ) ))
hostsa2poop=$(( $( cat $TDIR/bhostw.txt | grep eu-a2p | grep -v 'closed_Full\|ok' | wc -l ) ))

hosts_oop_total=$(( $hostsmsoop + $hostsa6oop + $hostsc7oop + $hostsfnoop + $hostsg1oop + $hostsg2oop + $hostsg3oop +$hostsa2poop ))
cores_oop_total=$(( $hostsmsoop*4 + $hostsa6oop*36 + $hostsc7oop*24 + $hostsfnoop*128 + $hostsg1oop*128 + $hostsg2oop*36 + $hostsg3oop*128 +$hostsa2poop*128 ))
oop_ms_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-ms | tr "\n" "," )
oop_a6_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-a6 | tr "\n" "," )
oop_c7_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-c7 | tr "\n" "," )
oop_fn_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-fn | tr "\n" "," )
oop_g1_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-g1 | tr "\n" "," )
oop_g2_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-g2 | tr "\n" "," )
oop_g3_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-g3 | tr "\n" "," )
oop_a2p_hn=$( cat $TDIR/bhostw.txt | grep -v 'closed_Full\|ok' | cut -d " " -f 1 | grep -v "eu-lsf\|HOST_NAME" | grep eu-a2p | tr "\n" "," )

# Total number of cores and hosts IP
hostsmsip=$(( $( cat $TDIR/bhostw.txt | grep eu-ms | grep 'closed_Full\|ok' | wc -l ) ))
hostsa6ip=$(( $( cat $TDIR/bhostw.txt | grep eu-a6 | grep 'closed_Full\|ok' | wc -l ) ))
hostsc7ip=$(( $( cat $TDIR/bhostw.txt | grep eu-c7 | grep 'closed_Full\|ok' | wc -l ) ))
hostsfnip=$(( $( cat $TDIR/bhostw.txt | grep eu-fn | grep 'closed_Full\|ok' | wc -l ) ))
hostsg1ip=$(( $( cat $TDIR/bhostw.txt | grep eu-g1 | grep 'closed_Full\|ok' | wc -l ) ))
hostsg2ip=$(( $( cat $TDIR/bhostw.txt | grep eu-g2 | grep 'closed_Full\|ok' | wc -l ) ))
hostsg3ip=$(( $( cat $TDIR/bhostw.txt | grep eu-g3 | grep 'closed_Full\|ok' | wc -l ) ))
hostsa2pip=$(( $( cat $TDIR/bhostw.txt | grep eu-a2p | grep 'closed_Full\|ok' | wc -l ) ))

hosts_ip_total=$(( $hostsmsip + $hostsa6ip + $hostsc7ip + $hostsfnip + $hostsg1ip + $hostsg2ip + $hostsg3ip + $hostsa2pip ))
cores_ip_total=$(( $hostsmsip*4 + $hostsa6ip*36 + $hostsc7ip*24 + $hostsfnip*128 + $hostsg1ip*128 + $hostsg2ip*36 + $hostsg3ip*128 + $hostsa2pip*128 ))

# Cores in use on Leoopen nodes
cores_in_use_leoopen=$( bhosts -w | grep eu-lo | awk '{ SUM += $6 } END {print SUM}' )

# Total number of cores in use
cores_in_use=$(( $( cat $TDIR/bqueues.txt | awk '{if (NR!=1) { SUM += $10 }} END {print SUM}' ) - $cores_in_use_leoopen ))

# Total number of pending jobslots
cores_pending=$( cat $TDIR/bqueues.txt | awk '{if (NR!=1) { SUM += $9 }} END {print SUM}' )

# Ratios
cores_oop_rat=`echo "scale=2; ($cores_oop_total*100)/$cores_total " | bc -l`
cores_oop_rat_overoip=`echo "scale=2; ($cores_oop_total*100)/$cores_ip_total " | bc -l`

cores_inuse_overip=`echo "scale=2; ($cores_in_use*100)/$cores_ip_total " | bc -l`

hosts_oop_rat=`echo "scale=2; ($hosts_oop_total*100)/$hosts_total " | bc -l`
hosts_oop_rat_overoip=`echo "scale=2; ($hosts_oop_total*100)/$hosts_ip_total " | bc -l`


# Oputputs
echo -e "\n*=============== LSF Cluster Availability ==============="
echo -e "| Hosts total                                  : $hosts_total"
echo -e "| Hosts in production                          : $hosts_ip_total"
echo -e "| Hosts out of production                      : $hosts_oop_total"
echo -e "*--------------------------------------------------------"
echo -e "| eu-ms out of production                      : $hostsmsoop out of $hostsms"
echo -e "| hostnames                                    : $oop_ms_hn"
echo -e "| empty eu-ms hosts                            : $emptyms out of $hostsms"
echo -e "*--------------------------------------------------------"
echo -e "| eu-c7 out of production                      : $hostsc7oop out of $hostsc7"
echo -e "| hostnames                                    : $oop_c7_hn"
echo -e "| empty eu-c7 hosts                            : $emptyc7 out of $hostsc7"
echo -e "*--------------------------------------------------------"
echo -e "| eu-a6 out of production                      : $hostsa6oop out of $hostsa6"
echo -e "| hostnames                                    : $oop_a6_hn"
echo -e "| empty eu-a6 hosts                            : $emptya6 out of $hostsa6"
echo -e "*--------------------------------------------------------"
echo -e "| eu-fn out of production                      : $hostsfnoop out of $hostsfn"
echo -e "| hostnames                                    : $oop_fn_hn"
echo -e "| empty eu-fn hosts                            : $emptyfn out of $hostsfn"
echo -e "*--------------------------------------------------------"
echo -e "| eu-g1 out of production                      : $hostsg1oop out of $hostsg1"
echo -e "| hostnames                                    : $oop_g1_hn"
echo -e "| empty eu-g1 hosts                            : $emptyg1 out of $hostsg1"
echo -e "*--------------------------------------------------------"
echo -e "| eu-g2 out of production                      : $hostsg2oop out of $hostsg2"
echo -e "| hostnames                                    : $oop_g2_hn"
echo -e "| empty eu-g2 hosts                            : $emptyg2 out of $hostsg2"
echo -e "*--------------------------------------------------------"
echo -e "| eu-g3 out of production                      : $hostsg3oop out of $hostsg3"
echo -e "| hostnames                                    : $oop_g3_hn"
echo -e "| empty eu-g3 hosts                            : $emptyg3 out of $hostsg3"
echo -e "*--------------------------------------------------------"
echo -e "| eu-a2p out of production                      : $hostsa2poop out of $hostsa2p"
echo -e "| hostnames                                    : $oop_a2p_hn"
echo -e "| empty eu-a2p hosts                            : $emptya2p out of $hostsa2p"
echo -e "*--------------------------------------------------------"
echo -e "| Cores total                                  : $cores_total"
echo -e "| Cores in production                          : $cores_ip_total"
echo -e "| Cores out of production                      : $cores_oop_total"
echo -e "*--------------------------------------------------------"
echo -e "| Cores in use                                 : $cores_in_use"
echo -e "| Cores available                              : $(( $cores_ip_total - $cores_in_use ))"
echo -e "| Pending job slots                            : $cores_pending"
echo -e "*--------------------------------------------------------"
echo -e "| Cores out of production % over total         : $cores_oop_rat%"
echo -e "| Cores out of production % over in prod       : $cores_oop_rat_overoip%"
echo -e "| Cores in use % over in production            : $cores_inuse_overip%"
echo -e "*--------------------------------------------------------"
echo -e "| Hosts out of production % over total         : $hosts_oop_rat%"
echo -e "| Hosts out of production % over in production : $hosts_oop_rat_overoip%"
echo -e "*========================================================\n"
